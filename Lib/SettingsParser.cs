﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Lib
{
	public class SettingsParser
	{
		public static List<string> ParseSettings(string stringtoparse)
		{
			MatchCollection matches = Regex.Matches(stringtoparse, "([.A-Za-z\\d\\s-\\\\\"_/'æøå=.]+)|([{]{1}[A-Za-z0-9]+[}]{1})");
			var querytoreturn = new List<string>();
			var queryindex = new List<int>();
			for (int i = 0; i < matches.Count; i++)
			{
				Match match = matches[i];
				if (!string.IsNullOrEmpty(match.Value))
				{
					if (match.Value.IndexOf('{') == 0 && match.Value.IndexOf('}') == match.Value.Length - 1)
					{
						queryindex.Add(i);
						querytoreturn.Add("%" + queryindex.Count + "%");
					}
					else
					{
						querytoreturn.Add(match.Value);
					}
				}
			}

			return querytoreturn;
		}
	}
}