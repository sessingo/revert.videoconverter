﻿using System.IO;
using System.Text.RegularExpressions;

namespace Lib
{
	public class IniParser
	{
		#region Nested type: IniFile

		/// <summary>
		/// Parses the settings from an ini from
		/// Usage: mysearchkey=myvalue
		/// </summary>
		public class IniFile
		{
			private static readonly Regex IniKeyValuePatternRegex;
			private readonly string _iniFileName;

			static IniFile()
			{
				IniKeyValuePatternRegex = new Regex(
					@"((\s)*(?<Key>([^\=^\s^\n]+))[\s^\n]*
            # key part (surrounding whitespace stripped)
            \=
            (\s)*(?<Value>([^\n^\s]+(\n){0,1})))
            # value part (surrounding whitespace stripped)
            ",
					RegexOptions.IgnorePatternWhitespace |
					RegexOptions.Compiled |
					RegexOptions.CultureInvariant);
			}

			public IniFile(string iniFileName)
			{
				_iniFileName = iniFileName;
			}

			public string IniFileName
			{
				get { return _iniFileName; }
			}

			public string ParseFileReadValue(string key)
			{
				using (var reader =
					new StreamReader(_iniFileName))
				{
					do
					{
						string line = reader.ReadLine();
						if (line != null)
						{
							Match match = IniKeyValuePatternRegex.Match(line);
							if (match.Success)
							{
								string currentKey = match.Groups["Key"].Value;
								if (currentKey.Trim().CompareTo(key) == 0) {
									var value = match.Groups["Value"].Value;
									return value;
								}
							}
						}
					} while (reader.Peek() != -1);
				}
				return null;
			}
		}

		#endregion
	}
}