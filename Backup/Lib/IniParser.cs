﻿using System.IO;
using System.Text.RegularExpressions;

namespace Lib
{
	public class IniParser
	{
		#region Nested type: IniFile

		/// <summary>
		/// Parses the settings from an ini from
		/// Usage: mysearchkey=myvalue
		/// </summary>
		public class IniFile
		{
			private static readonly Regex _iniKeyValuePatternRegex;
			private readonly string _iniFileName;

			static IniFile()
			{
				_iniKeyValuePatternRegex = new Regex(
					@"((\s)*(?<Key>([^\=^\s^\n]+))[\s^\n]*
            # key part (surrounding whitespace stripped)
            \=
            (\s)*(?<Value>([^\n^\s]+(\n){0,1})))
            # value part (surrounding whitespace stripped)
            ",
					RegexOptions.IgnorePatternWhitespace |
					RegexOptions.Compiled |
					RegexOptions.CultureInvariant);
			}

			public IniFile(string iniFileName)
			{
				_iniFileName = iniFileName;
			}

			public string IniFileName
			{
				get { return _iniFileName; }
			}

			public string ParseFileReadValue(string key)
			{
				using (StreamReader reader =
					new StreamReader(_iniFileName))
				{
					do
					{
						string line = reader.ReadLine();
						Match match =
							_iniKeyValuePatternRegex.Match(line);
						if (match.Success)
						{
							string currentKey =
								match.Groups["Key"].Value as string;
							if (currentKey != null &&
							    currentKey.Trim().CompareTo(key) == 0)
							{
								string value =
									match.Groups["Value"].Value as string;
								return value;
							}
						}
					} while (reader.Peek() != -1);
				}
				return null;
			}
		}

		#endregion
	}
}