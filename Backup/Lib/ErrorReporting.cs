﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Lib
{
	public class ErrorReporting
	{
		private static readonly List<string> errors = new List<string>();

		public static void RegisterError(string errmsg)
		{
			if (!string.IsNullOrEmpty(errmsg))
			{
				errors.Add(string.Format("{0}: {1}", DateTime.Now, errmsg));
			}
		}

		public static void WriteErrorLog()
		{
			// If the logfile exist and is greater than 500K/B then delete.
			FileInfo logfile =
				new FileInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6) +
				             "\\logfile.txt");
			if (logfile.Exists)
			{
				if (logfile.Length > 500*1024)
				{
					logfile.Delete();
				}
			}

			using (
				StreamWriter writer =
					new StreamWriter(
						Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6) + "\\logfile.txt", true,
						Encoding.Default, 400))
			{
				logfile.Refresh();
				if (logfile.Length < 10)
				{
					writer.WriteLine("[- VideoConverter Logfile -]");
					writer.WriteLine("");
					writer.WriteLine("By Simon Sessingø @ www.pecee.dk");
					writer.WriteLine("Copyright (C) 2008 | All rights reserved");
					writer.WriteLine("");
				}
				foreach (string error in errors)
				{
					if (error != null)
					{
						writer.WriteLine("VideoConverter " + error);
					}
				}

				writer.Dispose();
			}
		}
	}
}