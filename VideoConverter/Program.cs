﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;
using Lib;

namespace VideoConverter
{
	internal class Program
	{
		public static string Connectionstring { get; set; }
		public static string SelectToCheckConversion { get; set; }
		public static string UpdateOnConversionComplete { get; set; }
		public static string ConversionParameter1 { get; set; }
		public static string ConversionParameter2 { get; set; }

		private static void UpdateDatabase(int id)
		{
			if (id != 0)
			{
				var newquery = new StringBuilder();
				foreach (string word in SettingsParser.ParseSettings(VideoConverter.Default.UpdateOnConversionComplete))
				{
					switch (word)
					{
						case "%1%":
							newquery.Append(id);
							break;
						default:
							newquery.Append(word);
							break;
					}
				}

				using (var connection = new SqlConnection(Connectionstring))
				{
					try
					{
						connection.Open();
						using (var command = new SqlCommand (newquery.ToString(), connection))
						{
							command.ExecuteNonQuery();
						}
					}
					catch (Exception e)
					{
						ErrorReporting.RegisterError("Failed to update converting status to the database " + e.Message);
						Console.Write("WARNING: Failed to update converting status to the database.\n");
					}
				}
			}

			return;
		}

		private static void Main(string[] args)
		{
			if (args.Length <= 0)
			{
				ErrorReporting.RegisterError("No parameters. VideoConverter is exiting.");
				Console.Write("One or more parameters not found. Please try again.\nPress any key to exit.");
				ErrorReporting.WriteErrorLog();
				return;
			}
			Connectionstring = VideoConverter.Default.ConnectionString;
			SelectToCheckConversion = VideoConverter.Default.SelectToCheckConversion;
			UpdateOnConversionComplete = VideoConverter.Default.UpdateOnConversionComplete;
			ConversionParameter1 = VideoConverter.Default.ConversionParameter1;
			ConversionParameter2 = VideoConverter.Default.ConversionParameter2;

			ErrorReporting.RegisterError("Program started.");
			string pathtowatch = args[0];
			var directory = new DirectoryInfo(pathtowatch);
			if (!directory.Exists)
			{
				ErrorReporting.RegisterError("Directory " + pathtowatch + " not found. Please check parameters/database.");
				Console.Write("The directory " + pathtowatch + " was not fould.\nPress any key to exit.");
				ErrorReporting.WriteErrorLog();
			}

			var filestoconvert = new List<string>();
			var idstoconvert = new List<int>();

			using (var connection = new SqlConnection(Connectionstring))
			{
				try
				{
					connection.Open();
					using (var command = new SqlCommand(SelectToCheckConversion, connection))
					{
						using (SqlDataReader reader = command.ExecuteReader())
						{
							while (reader.Read())
							{
								idstoconvert.Add(reader.GetInt32(0));
								filestoconvert.Add(reader.GetString(1));
							}
						}
					}
				}
				catch (Exception e)
				{
					ErrorReporting.RegisterError(e.Message);
					Console.Write(
						"An error occured while executing query. See logfile.txt for more information.\nPress any key to exit.");
					ErrorReporting.WriteErrorLog();
					return;
				}
			}
			int id = 0;
			if (filestoconvert.Count > 0)
			{
				Console.WriteLine("Starting to convert " + filestoconvert.Count + " items...");
				ErrorReporting.RegisterError("Starting to convert " + filestoconvert.Count + " items...");
				foreach (string file in filestoconvert)
				{
					int outputfile;
					if (idstoconvert[id] > 0)
					{
						outputfile = (idstoconvert[id] - 1);
					}
					else
					{
						outputfile = 1;
					}
					var convertargs1 = new StringBuilder();
					foreach (string word in SettingsParser.ParseSettings(VideoConverter.Default.ConversionParameter1))
					{
						switch (word)
						{
							case "%1%":
								convertargs1.Append(directory);
								break;
							case "%2%":
								convertargs1.Append(file);
								break;
							case "%3%":
								convertargs1.Append(directory);
								break;
							case "%4%":
								convertargs1.Append(outputfile);
								break;
							default:
								convertargs1.Append(word);
								break;
						}
					}

					Console.WriteLine((id + 1) + "/" + filestoconvert.Count + " - Converting " + file + " to .flv");
					ErrorReporting.RegisterError((id + 1) + "/" + filestoconvert.Count + " - Converting " + file + " to .flv");
					var convertfile = new Process
					{
					    StartInfo =
					        {FileName = "ffmpeg.exe", Arguments = convertargs1.ToString(), UseShellExecute = false}
					};
					convertfile.Start();
					convertfile.WaitForExit(3000);

					ErrorReporting.RegisterError("Taking screenshot of " + file);
					Console.WriteLine("Taking screenshot of " + file);

					var convertargs2 = new StringBuilder();
					foreach (string word in SettingsParser.ParseSettings(VideoConverter.Default.ConversionParameter2))
					{
						switch (word)
						{
							case "%1%":
								convertargs2.Append(directory);
								break;
							case "%2%":
								convertargs2.Append(file);
								break;
							case "%3%":
								convertargs2.Append(directory);
								break;
							case "%4%":
								convertargs2.Append(outputfile);
								break;
							default:
								convertargs2.Append(word);
								break;
						}
					}

					var convertpicture = new Process
					{
					    StartInfo =
					        {FileName = "ffmpeg.exe", Arguments = convertargs2.ToString(), UseShellExecute = false}
					};
					convertpicture.Start();
					convertpicture.WaitForExit(3000); // Fixed timeout, if the program dosnt respont

					Console.Write("Converting complete!");
					ErrorReporting.RegisterError("Converting done (" + file + ")");

					var checkForFile = new FileInfo(directory + "\\" + outputfile + ".flv");
					if (checkForFile.Exists)
					{
						var filetodelete = new FileInfo(directory + "\\" + file);
						try
						{
							filetodelete.Delete();
							ErrorReporting.RegisterError("Deleted original-file: " + file + ". ");
						}
						catch (Exception e)
						{
							ErrorReporting.RegisterError("Error occured while deleting " + file + ". " + e);
						}
					}

					UpdateDatabase(idstoconvert[id]);

					id++;
				}
			}
			else
			{
				ErrorReporting.RegisterError("No items found to convert. Closing application.");
				Console.Write("No items found to convert. Closing...");
			}

			ErrorReporting.RegisterError("Program finished.");
			ErrorReporting.WriteErrorLog();
		}
	}
}